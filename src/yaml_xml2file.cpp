#include <fstream>
#include "jsonio17/jsondump.h"
#include "jsonimpex17/yaml_xml2file.h"
#include "jsonimpex17/yamldump.h"
#include "jsonimpex17/xmldump.h"


namespace jsonio17 {

//  JsonYamlXMLFile   --------------------------------------------------------------

TxtFile::FileTypes JsonYamlXMLFile::default_type = TxtFile::Json;

TxtFile::FileTypes JsonYamlXMLFile::type_from_extension(const std::string &ext)
{
    FileTypes type =  default_type;
    if( ext == "json" )
        type = Json;
    else  if( ext == "yaml" )
        type = Yaml;
    else if( ext == "xml" )
        type = XML;
    return type;
}

void JsonYamlXMLFile::set_type(TxtFile::FileTypes ftype)
{
    if( !( ftype == Json ||  ftype == Yaml || ftype == XML ) )
        JSONIO_THROW( "JsonYamlXMLFile", 10, " illegal file type." );
    file_type =  ftype;
}


void JsonYamlXMLFile::loadYaml( JsonBase &object ) const
{
    auto data_txt = TxtFile::load_all();
    yaml::loads( data_txt,  object );
}

void JsonYamlXMLFile::saveYaml( const JsonBase &object ) const
{
    std::fstream fout(file_path, std::ios::out );
    JSONIO_THROW_IF( !fout.good(), "JsonYamlXMLFile", 11, "file save error...  " + file_path );
    yaml::dump( fout, object );
}

void JsonYamlXMLFile::loadXml( JsonBase &object ) const
{
    auto data_txt = TxtFile::load_all();
    XML::loads( data_txt,  object );
}

void JsonYamlXMLFile::saveXml( const JsonBase &object ) const
{
    std::fstream fout(file_path, std::ios::out );
    JSONIO_THROW_IF( !fout.good(), "JsonYamlXMLFile", 12, "file save error...  " + file_path );
    XML::dump( fout, object );
}

void JsonYamlXMLFile::load( JsonBase& object ) const
{
    switch( file_type )
    {
    case FileTypes::Json:
        loadJson( object );
        break;
    case FileTypes::Yaml:
        loadYaml( object );
        break;
    case FileTypes::XML:
        loadXml( object );
        break;
    default:
        break;
    }
}

void JsonYamlXMLFile::save(const JsonBase& object) const
{
    switch( file_type )
    {
    case FileTypes::Json:
        saveJson(  object );
        break;
    case FileTypes::Yaml:
        saveYaml(  object );
        break;
    case FileTypes::XML:
        saveXml(  object );
        break;
    default:
        break;
    }
}


//  JsonYamlXMLArrayFile --------------------------------------------------------------------------


void JsonYamlXMLArrayFile::Close()
{
    if( open_mode == WriteOnly )
    {
        std::fstream fout(file_path, std::ios::out );
        JSONIO_THROW_IF( !fout.good(), "JsonYamlXMLFile", 15, " file save error...  " + file_path );
        save_to_stream( fout );
    }

    is_opened = false;
}


void JsonYamlXMLArrayFile::Open( TxtFile::OpenModeTypes amode )
{
    if( isOpened() )
    {
        JSONIO_THROW_IF( open_mode!=amode, "JsonYamlXMLFile", 16, "file was opened in different mode  " + file_path );
        return;
    }

    // clear settings
    open_mode  = amode;
    loaded_ndx = 0;
    arr_object.clear();

    if( open_mode == ReadOnly )
    {
        auto data_txt = TxtFile::load_all();
        read_from_string( data_txt );
    }
    else if( open_mode != WriteOnly )
    {
        JSONIO_THROW( "JsonYamlXMLFile", 17, " illegal file open mode." );
    }

    is_opened = true;
}

void JsonYamlXMLArrayFile::loadString( const std::string &data_string )
{
    if( isOpened() )
    {
        JSONIO_THROW( "JsonYamlXMLFile", 18,  "file was opened " + file_path );
        return;
    }

    // clear settings
    open_mode  = ReadOnly;
    loaded_ndx = 0;
    arr_object.clear();
    read_from_string( data_string );
}

std::string JsonYamlXMLArrayFile::getString()
{
    if( open_mode == WriteOnly )
    {
       std::stringstream fout;
       save_to_stream( fout );
       return fout.str();
    }
    return "";
}

void JsonYamlXMLArrayFile::set_type( TxtFile::FileTypes ftype )
{
    if( !( ftype == Json ||  ftype == Yaml || ftype == XML ) )
        JSONIO_THROW( "JsonYamlXMLFile", 11, " illegal file type." );
    file_type =  ftype;
}

void JsonYamlXMLArrayFile::read_from_string( const std::string &data_string )
{
    switch( file_type )
    {
    case FileTypes::Json:
        json::loads( data_string, arr_object );
        break;
    case FileTypes::Yaml:
        yaml::loads( data_string, arr_object );
        break;
    case FileTypes::XML:
        XML::loads( data_string, arr_object );
        break;
    default:
        break;
    }
}


void JsonYamlXMLArrayFile::save_to_stream( std::ostream& os )
{
    switch( file_type )
    {
    case FileTypes::Json:
        json::dump( os, arr_object );
        break;
    case FileTypes::Yaml:
        yaml::dump( os, arr_object );
        break;
    case FileTypes::XML:
        XML::dump( os, arr_object );
        break;
    default:
        break;
    }
}


} // namespace jsonio17

