
HEADERS += \
    $$JSONIMPEX17_DIR/lua-run.h \
    $$JSONIMPEX17_HEADERS_DIR/jsonimpex17/xmldump.h \
    $$JSONIMPEX17_HEADERS_DIR/jsonimpex17/yamldump.h \
    $$JSONIMPEX17_HEADERS_DIR/jsonimpex17/yaml_xml2file.h \
    $$JSONIMPEX17_HEADERS_DIR/jsonimpex17/thrift_impex.h \
    $$JSONIMPEX17_HEADERS_DIR/jsonimpex17/impex_generator.h \
    $$JSONIMPEX17_DIR/TJSONSerializer.h \
    $$JSONIMPEX17_DIR/thrift_ie_base.h \
    $$JSONIMPEX17_DIR/thrift_ie_json.h \
    $$JSONIMPEX17_DIR/thrift_ie_keyvalue.h \
    $$JSONIMPEX17_DIR/thrift_ie_table.h


SOURCES += \
    $$JSONIMPEX17_DIR/lua-run.cpp \
    $$JSONIMPEX17_DIR/xmldump.cpp \
    $$JSONIMPEX17_DIR/yamldump.cpp \
    $$JSONIMPEX17_DIR/yaml_xml2file.cpp \
    $$JSONIMPEX17_DIR/TJSONSerializer.cpp \
    $$JSONIMPEX17_DIR/thrift_ie_base.cpp \
    $$JSONIMPEX17_DIR/thrift_ie_json.cpp \
    $$JSONIMPEX17_DIR/thrift_ie_keyvalue.cpp \
    $$JSONIMPEX17_DIR/thrift_ie_table.cpp \
    $$JSONIMPEX17_DIR/thrift_impex.cpp \
    $$JSONIMPEX17_DIR/impex_generator.cpp


!contains(DEFINES, USE_THRIFT) {

HEADERS += \
    $$JSONIMPEX17_DIR/no_thrift/impex_base.h \
    $$JSONIMPEX17_DIR/no_thrift/impex_types_fix.h

SOURCES += \
    $$JSONIMPEX17_DIR/no_thrift/impex_types_fix.cpp

}
